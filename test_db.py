from kuhkal import models
from kuhkal import db

def main():
    db.create_all()
    recipe = models.Recipe(name="Pasulj")
    ing_1 = models.Ingredient(name="Meso", price=30)
    ing_2 = models.Ingredient(name="Luk", price=38)
    db.session.add(ing_1)
    db.session.add(ing_2)
    db.session.commit()
    ingredients = [ing_2, ing_1]
    for ing in ingredients:
        assoc = models.RecipeIngredientAssoc()
        assoc.ingredient = ing
        assoc.ingredient_id = ing.id
        assoc.ingredient_name= ing.name
        assoc.ingredient_price= ing.price
        assoc.recipe = recipe
        assoc.recipe_id = recipe.id
        assoc.ingredient_ammount = 30
    for ing in recipe.ingredients:
        print(ing.ingredient.name)
    db.session.add(recipe)
    db.session.commit()


if __name__ == "__main__":
    main()
    
    
