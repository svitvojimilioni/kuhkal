from . import db

class RecipeIngredientAssoc(db.Model):
    __tablename__ = "RecipeIngredientAssoc"
    recipe_id = db.Column(db.Integer, db.ForeignKey("recipe.id"), primary_key=True)
    ingredient_id = db.Column(
        db.Integer, db.ForeignKey("ingredient.id"), primary_key=True
    )
    ingredient_ammount = db.Column(db.Numeric(10, 2))
    ingredients = db.relationship("Ingredient", back_populates="recipe")
    recipe = db.relationship("Recipe", back_populates="ingredients")


class Recipe(db.Model):
    __tablename__ = "recipe"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    ingredients = db.relationship("RecipeIngredientAssoc", back_populates="recipe")


class Ingredient(db.Model):
    __tablename__ = "ingredient"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    price = db.Column(db.Numeric(10, 2))
    recipe = db.relationship("RecipeIngredientAssoc", back_populates="ingredients")
