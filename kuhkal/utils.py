from .models import Recipe, Ingredient
import typing as t
from . import db

def get_all_recipes() -> t.List:
    recipes = db.session.query(Recipe).all()
    all_recipes = []
    for recipe in recipes:
        ingredients = []
        for ingredient in recipe.ingredients:
            ingr = Ingredient.query.get(ingredient.ingredient_id)
            ingr_dict = {
                "name": ingr.name,
                "price": ingr.price,
                "ammount": ingredient.ingredient_ammount,
            }
            ingredients.append(ingr_dict)
        all_recipes.append({"recipe": recipe, "ingredients": ingredients})
    return all_recipes
